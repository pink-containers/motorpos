#!../../bin/linux-x86_64/sts

## You may have to change sts to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/sts.dbd"
sts_registerRecordDeviceDriver pdbbase

## test
#epicsEnvSet("BL", "LPQ")
#epicsEnvSet("DEV", "MPOS")

## docker
epicsEnvSet("BL", "$(IOCBL)")
epicsEnvSet("DEV", "$(IOCDEV)")

## Load record instances
#dbLoadRecords("db/xxx.db","user=epics")

cd "${TOP}/iocBoot/${IOC}"

dbLoadTemplate("msel.sub", BL=$(BL), DEV=$(DEV))

## autosave
set_savefile_path("/EPICS/autosave")
set_requestfile_path("${TOP}/iocBoot/${IOC}")
set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")

iocInit

## autosave
create_monitor_set("auto_settings.req", 30, "BL=$(BL), DEV=$(DEV)")

## Start any sequence programs
#seq sncxxx,"user=epics"
